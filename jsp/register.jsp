<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>

.gradient-custom {
  background: 	
#dae4f5;

.card-registration {
  font-size: 1rem;
  line-height: 2.15;
  padding-left: .75em;
  padding-right: .75em;
}
.card-registration .select-arrow {
  top: 13px;
}
</style>

</head>
<body>
	<header>
		<nav class="navbar navbar-dark navbar-expand-lg bg-dark text-white">
        	<div class="container-fluid py-1">
           	<a href="#" class="navbar-brand px-3">Record Book</a>
        	</div>
        	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-rb" aria-controls="navbar-rb" aria-expanded="false" aria-label="Toggle navigation">
            	<span class="navbar-toggler-icon"></span>
        	</button>

        	<div class="collapse navbar-collapse" id="navbar-rb">
            	<ul class="navbar-nav me-auto mb-2 mb-lg-0">
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="/MyRecordBook" class="nav-link text-white">Home</a>
                	</li>
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="register" class="nav-link text-white">Registration</a>
                	</li>
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="showrecords" class="nav-link text-white">Users</a>
                	</li>
            	</ul>
        	</div>
    	</nav>
	 </header>

	  <section class="vh-100 gradient-custom">
		<div class="container py-5 h-100">
		  <div class="row justify-content-center align-items-center h-100">
			<div class="col-12 col-lg-9 col-xl-7">
			  <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
				<div class="card-body p-4 p-md-5">
				  <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Registration Form</h3>
				  	<form:form id="regForm" modelAttribute="user" action="registerProcess"
						method="post">
					<div class="row">
					  <div class="col-md-6 mb-4">
						<div class="form-outline">
						  <input type="text" id="firstname" name="firstname" class="form-control form-control-lg" />
						  <label class="form-label" for="firstname">First Name</label>
						</div>  
					  </div>
					  <div class="col-md-6 mb-4">  
						<div class="form-outline">
						  <input type="text" id="lastname" name="lastname" class="form-control form-control-lg" />
						  <label class="form-label" for="lastname">Last Name</label>
						</div>
					  </div>
					</div>
					<div class="row">
					  <div class="col-md-6 mb-4 d-flex align-items-center">
						<div class="form-outline">
							<input type="text" id="phoneNumber" name="phonenumber" class="form-control form-control-lg" />
							<label class="form-label" for="phoneNumber">Phone Number</label>
						  </div>
					  </div>
					  <div class="col-md-6 mb-4">
	  
						<h6 class="mb-2 pb-1">Blood Type: </h6>
	  
						<div class="row">
							<div class="col-12">
			
							  <select class="select form-control-lg" name="bloodtype">
								<option value="1" disabled>Choose option</option>
								<option value="0Rh-">0Rh-</option>
								<option value="0Rh+">0Rh+</option>
								<option value="ARh-">ARh-</option>
								<option value="ARh+">ARh+</option>
								<option value="BRh-">BRh-</option>
								<option value="BRh+">BRh+</option>
								<option value="ABRh-">ABRh-</option>
								<option value="ABRh+">ABRh+</option>
							  </select>
							  <label class="form-label select-label"></label>
							</div>
						  </div>
					  </div>
					</div>
					<div class="row">
					  <div class="col">
						<div class="form-outline">
							<textarea class="form-control" id="address" name="address" rows="4"></textarea>
							<label class="form-label" for="address">Address</label>
						</div>
					  </div>
					</div>  
					<div class="mt-4 pt-2">
					  <input class="btn btn-outline-dark btn-lg"  data-bs-toggle="modal" data-bs-target="#myModal" value="Add User" />
					</div>
					<div class="modal" id="myModal">
  					<div class="modal-dialog">
    				<div class="modal-content">
    				
      				<div class="modal-header">
        			<h4 class="modal-title">Informations confirmed</h4>
        			<button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      				</div>

      				<div class="modal-body">
        			Do you confirm the registration process?
      				</div>

      				<!-- Modal footer -->
      				<div class="modal-footer">
        			<button type="submit" class="btn btn-outline-success" data-bs-dismiss="modal">Confirm</button>
      				</div>

    				</div>
  					</div>
					</div>
				  </form:form>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </section>
	   <footer>
    	<div class=" fixed-bottom">
    		 <div class="footer-copyright text-center py-1 bg-dark">text
  			</div>
    	</div>
    	</footer>

	   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>